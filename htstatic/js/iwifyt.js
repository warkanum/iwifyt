$(document).ready(function () {

    $("#btn_download").click(function (e) {
        $.post("/download", { "youtube_url": $("#text_youtube_url").val() })
            .done(function (json) {

                $("#feedback").html('<span class="success">Queued: ' + json.message + '</span>');
                $("#feedback").show();

                $("#details").html('<pre>' + json.detail + '</pre>');

                refreshStatus();

            })
            .fail(function (string) {

                $("#feedback").html('<span class="error">' + string + '</span>');
                $("#feedback").show();

            });
        e.preventDefault();
    });

    function refreshStatus() {
        $.get("/downloaded", function (data) {
            $("#status").html('<pre>' + data + '</pre>');
        });
    }

    $("#btn_refresh").click(function (e) {
        refreshStatus();
    });

    setInterval(function () {
        refreshStatus();
    }, 2000);

    
}); 