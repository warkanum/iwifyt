__author__ = 'warkanum'

import string
import cherrypy
import os, os.path
from M_IWIFYT.IWIFYT_Host import IWIFYTWeb
from M_IWIFYT.Tools import validate_password

    


class WIFYTSrv():
    
    def start(self):
        """
        When server starts
        """
    def cleanup(self):
        """
        When servers ends
        """


    def StartServer(self):
        conf = {
               '/': {
                   'tools.sessions.on': True,
                   'tools.staticdir.root': os.path.abspath(os.getcwd()),
                   'tools.auth_basic.on': True,
                   'tools.auth_basic.realm': 'localhost',
                   'tools.auth_basic.checkpassword': validate_password
               },
               '/static': {
                   'tools.staticdir.on': True,
                   'tools.staticdir.dir': './htstatic'
               }
         }
        
        cherrypy.config.update({'server.socket_host': '0.0.0.0',
                             'server.socket_port': 5061,
                             #'server.ssl_certificate': './ssl_test_cert.pem',
                             #'server.ssl_private_key': './ssl_test.key',
                            })
                            
        
        #cherrypy.engine.subscribe('start', WIFYTSrv.start(self))
        #cherrypy.engine.subscribe('stop', WIFYTSrv.cleanup(self))
        
        webapp = IWIFYTWeb()
        #webapp.generator = StringGeneratorWebService()
        cherrypy.quickstart(webapp, '/', conf)