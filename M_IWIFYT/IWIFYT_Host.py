import cherrypy
import json

from M_IWIFYT.Tools import readTextFile, callYoutubeDl, callDownloadList

class IWIFYTWeb(object):
    @cherrypy.expose
    def index(self):
        return readTextFile("htfiles/index.html")
    
    @cherrypy.expose
    def downloaded(self):
        cherrypy.response.headers['Content-Type'] = "text/plain"
        return callDownloadList()
    
    @cherrypy.expose
    def download(self,youtube_url=""):
        cherrypy.response.headers['Content-Type'] = "application/json"
        result = callYoutubeDl(youtube_url)
        
        message = {"message" : "URL: " + youtube_url
                   , "detail": result['detail']
                   , "status": result['status'] }
        
        return bytes(json.dumps(message), 'utf-8')

