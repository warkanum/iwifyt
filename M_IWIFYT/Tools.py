import subprocess
import configparser

def readTextFile(path):
    f = open(path, 'r')
    output = f.read()
    f.close()
    return output


def getSettings(name, default=''):
    config = configparser.ConfigParser()
    config.read('settings.cfg')

    if name == 'users':
        users = {}
        users = dict(config.items('users'))    
        return users
    elif name == 'downloader-script-path':
        return config['download']['script']
    elif name == 'download_folder':
        return config['download']['destination']
    elif name == 'lister-script-path':
        return config['download']['script_script']
    
    else:
        return default
    
def validate_password(realm, username, password):
        AuthUsers = getSettings('users', {})
        if username in AuthUsers and AuthUsers[username] == password:
            return True
        return False
    
    
def callYoutubeDl(url, settings={}):
    result = {"detail": "could not download: "+url, "status": "failed"}
    detail=""
    url = '"%s"' % url
    
    download_dir = getSettings('download_folder')
    EXECParms=[getSettings('downloader-script-path'), url, download_dir]
    
    proc = subprocess.Popen(EXECParms, stdout=subprocess.PIPE,cwd=download_dir)
    for line in proc.stdout.readlines():
        detail = detail + line.decode("utf-8").rstrip() + "\n"
    
    result = {"detail": detail, "status": "success"}
    return result

def callDownloadList(settings={}):
    detail=""

    download_dir = getSettings('download_folder')
    EXECParms=[getSettings('lister-script-path'), download_dir]
    
    proc = subprocess.Popen(EXECParms, stdout=subprocess.PIPE,cwd=download_dir)
    for line in proc.stdout.readlines():
        detail = detail + line.decode("utf-8").rstrip() + "\n"
    
    return detail
    