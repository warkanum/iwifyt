#!/bin/bash
echo Script Started...
URL="$1"
DEST="$2"
LOGFILE="download_log.txt"
DEBUGFILE="youtube-dl.log"

URL="${URL%\"}"
URL="${URL#\"}"

echo URL:$URL
echo DEST:$DEST

(
  echo IWFYT Youtube Download: $(date +"%y-%m-%d %H:%M:%S")
  echo URL: $URL
  
) >> $LOGFILE

cd $DEST$

echo History:
tail -n 2 youtube-dl.log
youtube-dl --format=22/18/35/34 --verbose $URL > $DEBUGFILE 2>&1 &

